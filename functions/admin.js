//


'use strict';

const admin = require('firebase-admin');

admin.initializeApp();

exports.store = admin.firestore();
exports.storage = admin.storage();


// EOF
