//


'use strict';

const GMaps = require('@googlemaps/google-maps-services-js');
const {store} = require('./admin');

const Functions = require('firebase-functions');
const FirebaseAdmin = require('firebase-admin');

const REGION = 'europe-west1';
const OPTIONS = {
    'timeoutSeconds': 30,
};

const funcs = Functions.region(REGION).runWith(OPTIONS);
const gMapsClient = new GMaps.Client({});
const gKey = Functions.config().ltp.google_api_key;

const logger = Functions.logger;

async function getNewPlaceId(poiId, oldPlaceId) {
    //
    let response = null;
    //
    const conf = {
        'params': {
            'fields': [
                'place_id',
            ],
            'key': gKey,
            'place_id': oldPlaceId,
        },
    };
    //
    try {
        response = await gMapsClient.placeDetails(conf);
    } catch (exc) {
        if (exc.response.status === GMaps.Status.NOT_FOUND) {
            logger.warn(
                `POI not found ${poiId} ${oldPlaceId}`,
                {'data': exc.response.data},
            );
        } else {
            logger.error(
                `Can not find POI ${poiId} ${oldPlaceId}`,
                {'data': exc.response.data},
            );
        }
    }
    //
    const newPlaceId = response?.data?.status === GMaps.Status.OK
        ? response.data.result.place_id
        : null;
    //
    return newPlaceId;
}

function writePlaceId(poiId, newPlaceId) {
    const poiRef = store.collection('pois').doc(poiId);
    return poiRef.update({
        'g_places_id': newPlaceId,
        'updated_at': FirebaseAdmin.firestore.FieldValue.serverTimestamp(),
    });
}

async function updatePlace(poiId, poi) { // eslint-disable-line max-statements
    //
    let changed = false;
    //
    const oldPlaceId = poi.g_places_id;
    //
    logger.debug(`Getting new Place ID (current: ${oldPlaceId}) ...`);
    let newPlaceId = await getNewPlaceId(poiId, oldPlaceId);
    //
    if (newPlaceId && newPlaceId !== oldPlaceId) {
        changed = true;
    } else {
        newPlaceId = oldPlaceId;
    }
    //
    logger.debug(`Writing place ID ${poiId} ${newPlaceId} ...`);
    await writePlaceId(poiId, newPlaceId);
    //
    const result = {
        'changed': changed,
        'new': newPlaceId,
        'old': oldPlaceId,
    };
    //
    return result;
}

async function onSchedule(_context) {
    const lim = 10;
    const now = new Date();
    // eslint-disable-next-line
    const offsetMilliSeconds = 7 * 24 * 60 * 60 * 1000; // 7 days
    const date = new Date(now.getTime() - offsetMilliSeconds);
    //
    const snap = await store.collection('pois')
        .where('updated_at', '<', date)
        .limit(lim)
        .get();
    //
    snap.forEach((doc) => {
        //
        const poiId = doc.id;
        const poi = doc.data();
        //
        logger.debug(`Updating POI ${doc.id} ...`);
        updatePlace(poiId, poi);
        //
    });
}

exports.schedule = funcs.pubsub.schedule('every 60 minutes').onRun(onSchedule);


// EOF
