//


'use strict';

const {Client} = require('@googlemaps/google-maps-services-js');
const {store} = require('./admin');

const functions = require('firebase-functions');
const admin = require('firebase-admin');

const REGION = 'europe-west1';
const OPTIONS = {
    'timeoutSeconds': 30,
};

const funcs = functions.region(REGION).runWith(OPTIONS);
const gMapsClient = new Client({});
const gKey = functions.config().ltp.google_api_key;

function buildMeta(poi) {
    const placeId = poi.g_places_id;
    const conf = {
        'params': {
            'fields': [
                'geometry/location',
            ],
            'key': gKey,
            'place_id': placeId,
        },
    };
    return gMapsClient.placeDetails(conf).then((response) => {
        const result = response.data.result;
        functions.logger.debug(`details ${placeId}`, result);
        const meta = {
            'geo': {
                'location': {
                    'latitude': result.geometry.location.lat,
                    'longitude': result.geometry.location.lng,
                },
            },
        };
        return meta;
    }).catch((err) => {
        functions.logger.error(err);
    });
}

function writeMeta(poiId, meta) {
    const poiRef = store.collection('pois').doc(poiId);
    return poiRef.update({
        '_meta': {
            'data': meta,
            'updated_at': admin.firestore.FieldValue.serverTimestamp(),
        },
    });
}

function doTrigger(poiId, poi) {
    let metaData = {};
    //
    const result = buildMeta(poi).then((meta) => {
        metaData = meta;
        functions.logger.debug(`buildMeta ${poiId} DONE`);
        return writeMeta(poiId, metaData);
    }).then(() => {
        functions.logger.debug(`writeMeta ${poiId} DONE`);
        functions.logger.info(`Trigger ${poiId} DONE`);
        return metaData;
    });
    return result;
}

function checkNeedsUpdate(change) {
    let needsUpdate = false;
    if (change.after.exists) {
        const newPoi = change.after.data();
        if (!('_meta' in newPoi)) {
            needsUpdate = true;
        } else if (change.before.exists) {
            const oldPoi = change.before.data();
            if (oldPoi.g_places_id !== newPoi.g_places_id) {
                needsUpdate = true;
            } else if (oldPoi.updated_at < newPoi.updated_at) {
                needsUpdate = true;
            }
        }
    }
    return needsUpdate;
}

const POI = 'pois/{poiId}';

exports.trigger = funcs.firestore.document(POI).onWrite((change, context) => {
    const poiId = context.params.poiId;
    functions.logger.info(`Trigger ${poiId} ...`);
    let result = null;
    //
    const needsUpdate = checkNeedsUpdate(change);
    if (needsUpdate === true) {
        const poi = change.after.data();
        result = doTrigger(poiId, poi);
    }
    //
    return result;
});


// EOF
