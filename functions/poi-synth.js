//


'use strict';

const {store, storage} = require('./admin');

const functions = require('firebase-functions');
const admin = require('firebase-admin');

const REGION = 'europe-west1';
const OPTIONS = {
    'timeoutSeconds': 30,
};

const funcs = functions.region(REGION).runWith(OPTIONS);

function getQuestions(questionnaire) {
    const questions = [];
    //
    for (const group of questionnaire.questions) {
        for (const question of group.questions) {
            questions.push(question);
        }
    }
    //
    return questions;
}

async function compilePictureNames(poiId, sheetId, questionUuid) {
    const names = [];
    //
    const bucket = storage.bucket();
    const prefix = `sheet-attachments/${poiId}/${sheetId}/${questionUuid}`;
    const options = {
        prefix,
    };
    //
    const [files] = await bucket.getFiles(options);
    files.forEach((file) => {
        names.push(file.name);
    });
    //
    return names;
}

async function compileAnswer(poiId, sheet, question) {
    const items = [];
    const uuid = question.uuid;
    const answers = sheet.sheet.answers ?? {};
    if (question.type === 'pictures') {
        const names = await compilePictureNames(poiId, sheet.id, uuid);
        items.push(...names);
    } else if (uuid in answers) {
        const answer = answers[uuid];
        items.push(answer);
    }
    return items;
}

async function compileAnswers(poiId, sheets, questions) {
    const compilation = {};
    //
    for (const sheet of sheets) {
        for (const question of questions) {
            // eslint-disable-next-line no-await-in-loop
            const items = await compileAnswer(poiId, sheet, question);
            if (items.length) {
                const uuid = question.uuid;
                if (!(uuid in compilation)) {
                    compilation[uuid] = [];
                }
                compilation[uuid].push(...items);
            }
        }
    }
    //
    return compilation;
}

function handleBoolean(values) {
    let answer = null;
    let counter = 0;
    for (const value of values) {
        if (value === true) {
            ++counter;
        } else if (value === false) {
            --counter;
        }
    }
    if (counter > 0) { // eslint-disable-line no-magic-numbers
        answer = true;
    } else if (counter < 0) { // eslint-disable-line no-magic-numbers
        answer = false;
    }
    return answer;
}

function handleSelectSingle(values) { // eslint-disable-line max-statements
    let answer = null;
    let max = 0;
    const counters = {};
    for (const value of values) {
        if (!(value in counters)) {
            counters[value] = 0;
        }
        ++counters[value];
    }
    for (const [value, counter] of Object.entries(counters)) {
        if (counter > max) {
            max = counter;
            answer = value;
        }
    }
    return answer;
}

function handleSelectMulti(values) {
    const answer = [];
    for (const value of values) {
        for (const choice of value) {
            if (!answer.includes(choice)) {
                answer.push(choice);
            }
        }
    }
    return answer;
}

function handlePictures(values) {
    const result = values;
    return result;
}

const categoryUuid = 'd4c0298c-7222-46f9-b09e-b1821cdcce53';

// eslint-disable-next-line max-statements
function doBuildSynth(questions, answers) {
    const handlers = {
        'boolean': handleBoolean,
        'pictures': handlePictures,
        'select_multi': handleSelectMulti,
        'select_single': handleSelectSingle,
    };
    const data = {
        '_report': {
            '_missing_answers': [],
            '_missing_category': null,
            '_missing_pictures': [],
        },
        'sheet': {},
    };
    for (const question of questions) {
        const uuid = question.uuid;
        if (uuid in answers) {
            const type = question.type;
            if (type in handlers) {
                data.sheet[uuid] = handlers[type](answers[uuid]);
            }
            if (uuid === categoryUuid) {
                // eslint-disable-next-line no-underscore-dangle,camelcase
                data._report._missing_category = false;
            }
        } else if (!('condition' in question)) {
            if (question.type === 'pictures') {
                // eslint-disable-next-line no-underscore-dangle
                data._report._missing_pictures.push(uuid);
            } else if (uuid === categoryUuid) {
                // eslint-disable-next-line no-underscore-dangle,camelcase
                data._report._missing_category = true;
            } else {
                // eslint-disable-next-line no-underscore-dangle
                data._report._missing_answers.push(uuid);
            }
        }
    }
    return data;
}

function buildSynth(poiId) {
    const questQuery = store.collection('questionnaires')
        .where('type', '==', 'poi')
        .orderBy('created_at', 'desc')
        .limit(1); // eslint-disable-line no-magic-numbers
    //
    let questions = null;
    //
    const promise = questQuery.get().then((questSnapshot) => {
        let questionnaire = null;
        questSnapshot.forEach((doc) => {
            questionnaire = doc.data();
        });
        return questionnaire;
    }).then((questionnaire) => {
        questions = getQuestions(questionnaire);
        //
        const poiRef = store.collection('pois').doc(poiId);
        const sheetsQuery = poiRef.collection('sheets')
            .orderBy('created_at', 'desc');
        //
        const sheetsSnapshot = sheetsQuery.get();
        return sheetsSnapshot;
    }).then((sheetsSnapshot) => {
        const sheets = [];
        sheetsSnapshot.forEach((sheetDoc) => {
            const sheet = {
                'id': sheetDoc.id,
                'sheet': sheetDoc.data(),
            };
            sheets.push(sheet);
        });
        const answers = compileAnswers(poiId, sheets, questions);
        return answers;
    }).then((answers) => {
        const data = doBuildSynth(questions, answers);
        return data;
    }).catch((exc) => {
        return functions.logger.error(exc);
    });
    return promise;
}

function writeSynth(poiId, data) {
    const poiRef = store.collection('pois').doc(poiId);
    return poiRef.update({
        '_synth': {
            'computed_at': admin.firestore.FieldValue.serverTimestamp(),
            'data': {
                // eslint-disable-next-line no-underscore-dangle
                '_report': data._report,
            },
            'sheet': data.sheet,
            'updated_at': admin.firestore.FieldValue.serverTimestamp(),
        },
    });
}

const SHEET = 'pois/{poiId}/sheets/{sheetId}';

exports.trigger = funcs.firestore.document(SHEET).onWrite((change, context) => {
    let data = null;
    //
    const poiId = context.params.poiId;
    functions.logger.info(`Synth trigger ${poiId} ...`);
    //
    const promise = buildSynth(poiId).then((synthData) => {
        functions.logger.debug(`buildSynth ${poiId} DONE`);
        data = synthData;
        return writeSynth(poiId, data);
    }).then(() => {
        functions.logger.debug(`writeSynth ${poiId} DONE`);
        functions.logger.info(`Synth trigger ${poiId} DONE`);
        return data;
    });
    return promise;
});


// EOF
