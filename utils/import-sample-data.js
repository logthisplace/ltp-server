//


'use strict';

const FirebaseAdmin = require('firebase-admin');

const pois = require('../samples/pois.json');

const PICTURE_UUID = '8389d3f1-5475-4301-a271-a3926d4b79c3';

async function addPicture(poiId, sheetId, questionUuid) {
    const storage = FirebaseAdmin.storage();
    const bucket = storage.bucket();
    const fileName = 'picture.jpeg';
    const localFilePath = `./samples/${fileName}`;
    const prefix = `sheet-attachments/${poiId}/${sheetId}/${questionUuid}`;
    const remoteFilePath = `${prefix}/${fileName}`;
    await bucket.upload(
        localFilePath,
        {
            'destination': remoteFilePath,
        },
    );
}

async function addPoi(poi) {
    //
    const store = FirebaseAdmin.firestore();
    //
    const poiDoc = await store.collection('pois').add({
        'created_at': FirebaseAdmin.firestore.FieldValue.serverTimestamp(),
        'g_places_id': poi.g_places_id,
        'updated_at': FirebaseAdmin.firestore.FieldValue.serverTimestamp(),
    });
    //
    for (const sheet of poi.sheets) {
        //
        const sheetDoc = poiDoc.collection('sheets').doc();
        //
        addPicture(poiDoc.id, sheetDoc.id, PICTURE_UUID);
        //
        // eslint-disable-next-line no-await-in-loop
        await sheetDoc.set({
            'answers': sheet,
            'created_at': FirebaseAdmin.firestore.FieldValue.serverTimestamp(),
            'updated_at': FirebaseAdmin.firestore.FieldValue.serverTimestamp(),
        });
    }
}

function main() {
    //
    FirebaseAdmin.initializeApp();
    //
    for (const poi of pois.data) {
        addPoi(poi);
    }
}

main();


// EOF
