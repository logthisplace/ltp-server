//


'use strict';

const FirebaseAdmin = require('firebase-admin');

const poiQuestionnaire = require('../samples/poi-questionnaire.json');
const userQuestionnaire = require('../samples/user-questionnaire.json');

const poiDetailsIconsLogic = require('../samples/poi-details-icons-logic.json');
const poiUserColorLogic = require('../samples/poi-user-color-logic.json');

const postalCodes = require('../samples/postal-codes.json');

async function main() {
    //
    FirebaseAdmin.initializeApp();
    //
    const store = FirebaseAdmin.firestore();
    //
    await store.collection('questionnaires').add({
        'created_at': FirebaseAdmin.firestore.FieldValue.serverTimestamp(),
        'questions': poiQuestionnaire,
        'type': 'poi',
    });
    await store.collection('questionnaires').add({
        'created_at': FirebaseAdmin.firestore.FieldValue.serverTimestamp(),
        'questions': userQuestionnaire,
        'type': 'user',
    });
    //
    await store.collection('poi_details_icons_logic').add({
        'created_at': FirebaseAdmin.firestore.FieldValue.serverTimestamp(),
        'data': poiDetailsIconsLogic,
    });
    await store.collection('poi_user_color_logic').add({
        'created_at': FirebaseAdmin.firestore.FieldValue.serverTimestamp(),
        'data': poiUserColorLogic,
    });
    //
    await store.collection('postal_codes').add({
        'created_at': FirebaseAdmin.firestore.FieldValue.serverTimestamp(),
        'data': postalCodes,
    });
}

main();


// EOF
