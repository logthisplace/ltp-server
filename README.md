# LTP server components

The server-side components of the LTP project rely entirely on the [Firebase](https://firebase.google.com/docs/firestore/) platform.
In particular the following services and APIs are used:

* [Cloud Firestore](https://firebase.google.com/docs/firestore/) as database
* [Firebase Authentication](https://firebase.google.com/docs/auth)
* [Cloud Functions for Firebase](https://firebase.google.com/docs/functions)
* [Cloud Storage for Firebase](https://firebase.google.com/docs/storage)
* [Firebase Hosting](https://firebase.google.com/docs/hosting) (optional)

Additionally [Google Maps Platform's Place Details API](https://developers.google.com/maps/documentation/places/web-service/details) is used.


## Firebase emulators

With [Firebase Local Emulator Suite](https://firebase.google.com/docs/emulator-suite) it is possible to emulate locally some of the Firebase APIs and services.


### Non-Firebase APIs

Some services and API calls can not be emulated.
In particular calls to the Google Maps API and the Google Places API.
So connecting to the real Google Maps servers is necessary and real API keys are required.

Both the Cloud Functions and the web application need to make calls to those APIs.


#### Web app

For the web app, add `GOOGLE_MAPS_API_KEY` to a `.env` file in the root directory.
Webpack will read this value thanks to `dotenv` library and substitute the value in the web app source code.

```
GOOGLE_MAPS_API_KEY=AIzaSy...
```

If this environment variable is not defined, the web app will use the same API key as the one used to connect to Firebase (which is obtained from the URL `/__/firebase/init.json`).


#### Cloud Functions

The Cloud Functions are written so that the API key is obtained from a normal Firebase Cloud Functions configuration variable `ltp.google_api_key`.
So when the Cloud Functions are running from the emulator,
there should be a `functions/.runtimeconfig.json` file containing:

```
{
  "ltp": {
    "google_api_key": "AIzaSy..."
  }
}
```

[Reference](https://firebase.google.com/docs/functions/local-emulator#set_up_functions_configuration_optional)


### Scripts

There are some helper scripts defined in `package.json` to ease use of the Firebase emulators:

* `npm run emulators:watch`
* `npm run emulators:start`
* `npm run emulators:push-config && npm run emulators:push-data`


## Deployment

Deploy according to normal Firebase deployment procedures.


### Before deployment

#### API key for Cloud Functions

Cloud Functions need to make calls to Google Maps/Place API.
A separate API key is needed.
Cloud Functions are written to read this API key from a normal Cloud Functions configuration variable named `ltp.google_api_key`:

```
firebase functions:config:set ltp.google_api_key=AIzaSy...
```

[Reference](https://firebase.google.com/docs/functions/config-env)


### Deployment

```
firebase deploy
```

### Google APIs

#### Web app

The web app uses the following APIs and they should be enabled on its API key:

* Cloud Firestore API
* Maps JavaScript API
* Places API


#### iOS app

The iOS app uses the following APIs and they should be enabled on its API key:

* Cloud Firestore API
* Cloud Storage for Firebase API
* Firebase Installations API
* Firebase Remote Config API
* Identity Toolkit API
* Maps SDK for iOS
* Places API
* Token Service API


## Cloud functions

Some details about the core cloud functions...


### POI place

This function is triggered on a schedule (every 60 minutes).

The goal of this function is to keep the Google Place ID of each POI up-to-date.
The Google Place ID of a specific POI can change over time, so it is important to regularly ask the Google Place API for the latest ID.

The function queries 10 POI documents whose update timestamp is older than 7 days. For each found document the function then queries the Google Place Details API to obtain the current Place ID for this POI. In any case the Place ID is written and the update timestamp is updated.

The following values might need to be adjusted over time to take into account the total amount of POI documents in the database:

* running every 60 minutes
* update timestamp older than 7 days
* limiting to block of 10 POIs at once


### POI meta

This function is triggered on "write" events on the POI documents.

The goal of this function is to keep the geo-coordinates of each POI up-to-date. The Google Maps terms of services allows to keep geo-coordinates for up to 30 consecutive days. Since the Place ID is updated on a schedule, the POI documents are regularly written and this function is triggered as a consequence on the same schedule.

The function first checks that either the Google Place ID or the update timestamp of the POI document has changed. If it is the case, then the function proceeds to query the geo-coordinates from Google Place Details API and writes the coordinates in the `_meta.data.geo.location` field as well as updates its `_meta.updated_at` field.

### POI synth

This function is triggered on "write" events on the nested POI answer sheets documents.

The goal of this function is to calculate the synthetic answer sheet of a POI. The synthetic answer sheet is written in the `_synth.sheet` field of the POI document.

As a side goal, the function also gathers a report of missing answers and pictures for the POI. This report is written in the `_synth.data._report` field of the POI document.
