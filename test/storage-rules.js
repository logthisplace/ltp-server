//


'use strict';

const FirebaseTesting = require('@firebase/rules-unit-testing');
const Storage = require('firebase/storage');

const appConfig = {};
const testUserId = 'TestUser';
const otherUserId = 'OtherUser';

let testEnv = null;

function getStorage(userId) {
    return testEnv.authenticatedContext(userId).storage();
}

function getUnauthedStorage() {
    return testEnv.unauthenticatedContext().storage();
}

function buildMeta(userId) {
    return {
        'contentType': 'image/jpeg',
        'customMetadata': {
            'owner_uid': userId,
        },
    };
}

before(async () => {
    testEnv = await FirebaseTesting.initializeTestEnvironment(appConfig);
});

after(async () => {
    await testEnv.cleanup();
});

beforeEach(async () => {
    await testEnv.clearStorage();
});

// eslint-disable-next-line max-lines-per-function
describe('Storage rules', () => {
    //
    const path = 'sheet-attachments/poiId/sheetId/questionUuid/foo.jpg';
    const bytes = new Uint8Array([0, 0, 0]);
    //
    // eslint-disable-next-line max-lines-per-function
    describe('Answer sheet attachments', () => {
        it('Allow create answer sheet attachment', async () => {
            const storage = getStorage(testUserId);
            const ref = Storage.ref(storage, path);
            const meta = buildMeta(testUserId);
            //
            await FirebaseTesting.assertSucceeds(
                Storage.uploadBytes(ref, bytes, meta),
            );
        });
        it('Deny create if incorrect content type', async () => {
            const storage = getStorage(testUserId);
            const ref = Storage.ref(storage, path);
            const meta = {
                'customMetadata': {
                    'owner_uid': testUserId,
                },
            };
            //
            await FirebaseTesting.assertFails(
                Storage.uploadBytes(ref, bytes, meta),
            );
        });
        it('Deny create if not authenticated', async () => {
            const storage = getUnauthedStorage();
            const ref = Storage.ref(storage, path);
            const meta = buildMeta(testUserId);
            //
            await FirebaseTesting.assertFails(
                Storage.uploadBytes(ref, bytes, meta),
            );
        });
        it('Deny create if incorrect owner UID', async () => {
            const storage = getStorage(testUserId);
            const ref = Storage.ref(storage, path);
            const meta = buildMeta(otherUserId);
            //
            await FirebaseTesting.assertFails(
                Storage.uploadBytes(ref, bytes, meta),
            );
        });
        it('Deny create if missing owner UID', async () => {
            const storage = getStorage(testUserId);
            const ref = Storage.ref(storage, path);
            const meta = {'contentType': 'image/jpeg'};
            //
            await FirebaseTesting.assertFails(
                Storage.uploadBytes(ref, bytes, meta),
            );
        });
        it('Deny create if too big', async () => {
            const storage = getStorage(testUserId);
            const ref = Storage.ref(storage, path);
            const maxSize = (20 * 1024 * 1024) - 1;
            const meta = buildMeta(testUserId);
            let dataBytes = null;
            //
            dataBytes = new Uint8Array(maxSize + 1);
            await FirebaseTesting.assertFails(
                Storage.uploadBytes(ref, dataBytes, meta),
            );
            dataBytes = new Uint8Array(maxSize);
            await FirebaseTesting.assertSucceeds(
                Storage.uploadBytes(ref, dataBytes, meta),
            );
        });
        it('Allow read for anyone', async () => {
            const storage = getUnauthedStorage();
            const ref = Storage.ref(storage, path);
            //
            await testEnv.withSecurityRulesDisabled(async (context) => {
                await Storage.uploadBytes(
                    Storage.ref(context.storage(), path),
                    bytes,
                    buildMeta(testUserId),
                );
            });
            //
            await FirebaseTesting.assertSucceeds(
                Storage.getBytes(ref),
            );
        });
        it('Deny update owner UID', async () => {
            const storage = getStorage(testUserId);
            const ref = Storage.ref(storage, path);
            const meta = {'customMetadata': {'owner_uid': testUserId}};
            //
            await testEnv.withSecurityRulesDisabled(async (context) => {
                await Storage.uploadBytes(
                    Storage.ref(context.storage(), path),
                    bytes,
                    buildMeta(otherUserId),
                );
            });
            //
            await FirebaseTesting.assertFails(
                Storage.updateMetadata(ref, meta),
            );
            await FirebaseTesting.assertFails(
                Storage.updateMetadata(ref, {}),
            );
        });
        it('Deny delete', async () => {
            const storage = getStorage(testUserId);
            const ref = Storage.ref(storage, path);
            //
            await testEnv.withSecurityRulesDisabled(async (context) => {
                await Storage.uploadBytes(
                    Storage.ref(context.storage(), path),
                    bytes,
                    buildMeta(testUserId),
                );
            });
            //
            await FirebaseTesting.assertSucceeds(
                Storage.getBytes(ref),
            );
            await FirebaseTesting.assertFails(
                Storage.deleteObject(ref),
            );
        });
    });
});


// EOF
