//


'use strict';

module.exports = {
    'env': {
        'es6': true,
        'mocha': true,
        'node': true,
    },
    'extends': 'eslint:all',
    'parserOptions': {
        'ecmaVersion': 'latest',
    },
    'reportUnusedDisableDirectives': true,
    'root': true,
    'rules': {
        'array-bracket-newline': ['error', 'consistent'],
        'array-element-newline': ['error', 'consistent'],
        'arrow-body-style': ['error', 'always'],
        'comma-dangle': ['error', 'always-multiline'],
        'dot-location': ['error', 'property'],
        'func-style': ['error', 'declaration'],
        'function-call-argument-newline': ['error', 'consistent'],
        'function-paren-newline': ['error', 'consistent'],
        'multiline-comment-style': ['error', 'starred-block'],
        'newline-per-chained-call': ['off'],
        'no-extra-parens': ['error', 'all', {'nestedBinaryExpressions': false}],
        'no-magic-numbers': ['off'],
        'no-plusplus': ['off'],
        'no-ternary': ['off'],
        'no-unused-vars': ['error', {'argsIgnorePattern': '^_'}],
        'object-shorthand': ['error', 'consistent-as-needed'],
        'one-var': ['error', 'never'],
        'operator-linebreak': ['error', 'before'],
        'padded-blocks': ['error', 'never'],
        'prefer-destructuring': ['off'],
        'quotes': ['error', 'single'],
        'space-before-function-paren': ['error', {
            'asyncArrow': 'always',
            'named': 'never',
        }],
    },
};


// EOF
