//


/* eslint-disable max-lines */

'use strict';

const FirebaseTesting = require('@firebase/rules-unit-testing');
const Firestore = require('firebase/firestore');

const appConfig = {};
const testUserId = 'TestUser';
const otherUserId = 'OtherUser';

let testEnv = null;

function getFirestore(userId) {
    const context = testEnv.authenticatedContext(userId);
    return context.firestore();
}

function getUnauthedFirestore() {
    const context = testEnv.unauthenticatedContext();
    return context.firestore();
}

// eslint-disable-next-line max-lines-per-function
describe('Firestore rules', () => {
    //
    before(async () => {
        Firestore.setLogLevel('error');
        testEnv = await FirebaseTesting.initializeTestEnvironment(appConfig);
    });
    after(async () => {
        await testEnv.cleanup();
    });
    beforeEach(async () => {
        await testEnv.clearFirestore();
    });
    //
    describe('Constants', () => {
        const collectionNames = [
            'poi_details_icons_logic',
            'poi_user_color_logic',
            'postal_codes',
            'questionnaires',
        ];
        collectionNames.forEach((name) => {
            describe(`Collection "${name}"`, () => {
                it(`Allow unauthenticated query "${name}"`, async () => {
                    const query = Firestore.query(
                        Firestore.collection(getUnauthedFirestore(), name),
                    );
                    await FirebaseTesting.assertSucceeds(
                        Firestore.getDocs(query),
                    );
                });
                it(`Deny create document in "${name}"`, async () => {
                    const store = getFirestore(testUserId);
                    const collection = Firestore.collection(store, name);
                    await FirebaseTesting.assertFails(
                        Firestore.addDoc(collection, {'foo': 'bar'}),
                    );
                });
                it(`Deny delete document in "${name}"`, async () => {
                    const path = `${name}/abc123`;
                    await testEnv.withSecurityRulesDisabled(async (context) => {
                        await Firestore.setDoc(
                            Firestore.doc(context.firestore(), path),
                            {'foo': 'bar'},
                        );
                    });
                    const doc = Firestore.doc(getFirestore(testUserId), path);
                    await FirebaseTesting.assertFails(
                        Firestore.deleteDoc(doc),
                    );
                });
            });
        });
    });
    //
    describe('Log', () => {
        it('Allow create log document', async () => {
            const store = getFirestore(testUserId);
            const collection = Firestore.collection(store, 'log');
            const data = {
                'client_id': 'ios_app',
                'created_at': Firestore.serverTimestamp(),
                'level': 'a',
                'message': 'b',
                'user_id': testUserId,
            };
            await FirebaseTesting.assertSucceeds(
                Firestore.addDoc(collection, data),
            );
        });
        it('Deny query log documents', async () => {
            const query = Firestore.query(
                Firestore.collection(getFirestore(testUserId), 'log'),
            );
            await FirebaseTesting.assertFails(
                Firestore.getDocs(query),
            );
        });
    });
    //
    describe('POI', () => {
        const path = 'pois/doc123';
        const data = {
            'created_at': Firestore.serverTimestamp(),
            'g_places_id': 'random',
            'updated_at': Firestore.serverTimestamp(),
        };
        it('Allow create POI document', async () => {
            const store = getFirestore(testUserId);
            const collection = Firestore.collection(store, 'pois');
            await FirebaseTesting.assertSucceeds(
                Firestore.addDoc(collection, data),
            );
        });
        it('Allow unauthenticated query POI', async () => {
            const store = getUnauthedFirestore();
            const query = Firestore.query(Firestore.collection(store, 'pois'));
            await FirebaseTesting.assertSucceeds(
                Firestore.getDocs(query),
            );
        });
        it('Deny update POI', async () => {
            await testEnv.withSecurityRulesDisabled(async (context) => {
                await Firestore.setDoc(
                    Firestore.doc(context.firestore(), path),
                    data,
                );
            });
            const docRef = Firestore.doc(getFirestore(testUserId), path);
            const updateData = {'updated_at': Firestore.serverTimestamp()};
            await FirebaseTesting.assertFails(
                Firestore.updateDoc(docRef, updateData),
            );
        });
        it('Deny delete POI', async () => {
            await testEnv.withSecurityRulesDisabled(async (context) => {
                await Firestore.setDoc(
                    Firestore.doc(context.firestore(), path),
                    data,
                );
            });
            const docRef = Firestore.doc(getFirestore(testUserId), path);
            await FirebaseTesting.assertFails(
                Firestore.deleteDoc(docRef),
            );
        });
    });
    //
    // eslint-disable-next-line max-lines-per-function
    describe('POI answer sheets', () => {
        const sheetsPath = 'pois/poi123/sheets';
        const sheetPath = `${sheetsPath}/sheet123`;
        describe('Create', () => {
            it('Allow create POI answer sheet', async () => {
                const data = {
                    'answers': {},
                    'created_at': Firestore.serverTimestamp(),
                    'device_uuid': 'a',
                    'updated_at': Firestore.serverTimestamp(),
                    'user_id': testUserId,
                };
                const store = getFirestore(testUserId);
                const collection = Firestore.collection(store, sheetsPath);
                await FirebaseTesting.assertSucceeds(
                    Firestore.addDoc(collection, data),
                );
            });
            it('Deny create POI answer sheet other user', async () => {
                const data = {
                    'answers': {},
                    'created_at': Firestore.serverTimestamp(),
                    'device_uuid': 'a',
                    'updated_at': Firestore.serverTimestamp(),
                    'user_id': otherUserId,
                };
                const store = getFirestore(testUserId);
                const collection = Firestore.collection(store, sheetsPath);
                await FirebaseTesting.assertFails(
                    Firestore.addDoc(collection, data),
                );
            });
        });
        describe('Read', () => {
            it('Allow unauthenticated list sheets', async () => {
                const query = Firestore.query(
                    Firestore.collectionGroup(getUnauthedFirestore(), 'sheets'),
                );
                await FirebaseTesting.assertSucceeds(
                    Firestore.getDocs(query),
                );
            });
        });
        describe('Update', () => {
            it('Allow update own POI answer sheet', async () => {
                await testEnv.withSecurityRulesDisabled(async (context) => {
                    await Firestore.setDoc(
                        Firestore.doc(context.firestore(), sheetPath),
                        {'user_id': testUserId},
                    );
                });
                const doc = Firestore.doc(getFirestore(testUserId), sheetPath);
                const data = {'updated_at': Firestore.serverTimestamp()};
                await FirebaseTesting.assertSucceeds(
                    Firestore.updateDoc(doc, data),
                );
            });
            it('Deny update other POI answer sheet', async () => {
                await testEnv.withSecurityRulesDisabled(async (context) => {
                    await Firestore.setDoc(
                        Firestore.doc(context.firestore(), sheetPath),
                        {'user_id': otherUserId},
                    );
                });
                const doc = Firestore.doc(getFirestore(testUserId), sheetPath);
                const data = {'updated_at': Firestore.serverTimestamp()};
                await FirebaseTesting.assertFails(
                    Firestore.updateDoc(doc, data),
                );
            });
        });
        describe('Delete', () => {
            it('Deny delete POI answer sheet', async () => {
                await testEnv.withSecurityRulesDisabled(async (context) => {
                    await Firestore.setDoc(
                        Firestore.doc(context.firestore(), sheetPath),
                        {'user_id': testUserId},
                    );
                });
                const doc = Firestore.doc(getFirestore(testUserId), sheetPath);
                await FirebaseTesting.assertFails(
                    Firestore.deleteDoc(doc),
                );
            });
        });
    });
    // eslint-disable-next-line max-lines-per-function
    describe('User profile', () => {
        function buildData(userId) {
            return {
                'answers': {},
                'created_at': Firestore.serverTimestamp(),
                'device_uuid': 'abc',
                'updated_at': Firestore.serverTimestamp(),
                'user_id': userId,
            };
        }
        const path = 'users/abc123';
        describe('Create', () => {
            it('Allow create user profile', async () => {
                const data = buildData(testUserId);
                const store = getFirestore(testUserId);
                const collection = Firestore.collection(store, 'users');
                await FirebaseTesting.assertSucceeds(
                    Firestore.addDoc(collection, data),
                );
            });
            it('Deny create other user profile', async () => {
                const data = buildData(otherUserId);
                const store = getFirestore(testUserId);
                const collection = Firestore.collection(store, 'users');
                await FirebaseTesting.assertFails(
                    Firestore.addDoc(collection, data),
                );
            });
        });
        describe('Read', () => {
            it('Allow read own user profile', async () => {
                await testEnv.withSecurityRulesDisabled(async (context) => {
                    await Firestore.addDoc(
                        Firestore.collection(context.firestore(), 'users'),
                        buildData(testUserId),
                    );
                });
                const query = Firestore.query(
                    Firestore.collection(getFirestore(testUserId), 'users'),
                    Firestore.where('user_id', '==', testUserId),
                );
                await FirebaseTesting.assertSucceeds(
                    Firestore.getDocs(query),
                );
            });
            it('Deny read other user profile', async () => {
                await testEnv.withSecurityRulesDisabled(async (context) => {
                    await Firestore.addDoc(
                        Firestore.collection(context.firestore(), 'users'),
                        buildData(otherUserId),
                    );
                });
                const query = Firestore.query(
                    Firestore.collection(getFirestore(testUserId), 'users'),
                    Firestore.where('user_id', '==', otherUserId),
                );
                await FirebaseTesting.assertFails(
                    Firestore.getDocs(query),
                );
            });
            it('Deny query all user profiles', async () => {
                const query = Firestore.query(
                    Firestore.collection(getFirestore(testUserId), 'users'),
                );
                await FirebaseTesting.assertFails(
                    Firestore.getDocs(query),
                );
            });
        });
        describe('Update', () => {
            const data = {'updated_at': Firestore.serverTimestamp()};
            it('Allow update own user profile', async () => {
                await testEnv.withSecurityRulesDisabled(async (context) => {
                    await Firestore.setDoc(
                        Firestore.doc(context.firestore(), path),
                        buildData(testUserId),
                    );
                });
                const doc = Firestore.doc(getFirestore(testUserId), path);
                await FirebaseTesting.assertSucceeds(
                    Firestore.updateDoc(doc, data),
                );
            });
            it('Deny update other user profile', async () => {
                await testEnv.withSecurityRulesDisabled(async (context) => {
                    await Firestore.setDoc(
                        Firestore.doc(context.firestore(), path),
                        buildData(otherUserId),
                    );
                });
                const doc = Firestore.doc(getFirestore(testUserId), path);
                await FirebaseTesting.assertFails(
                    Firestore.updateDoc(doc, data),
                );
            });
        });
        describe('Delete', () => {
            it('Deny delete own user profile', async () => {
                await testEnv.withSecurityRulesDisabled(async (context) => {
                    await Firestore.setDoc(
                        Firestore.doc(context.firestore(), path),
                        buildData(testUserId),
                    );
                });
                const doc = Firestore.doc(getFirestore(testUserId), path);
                await FirebaseTesting.assertFails(
                    Firestore.deleteDoc(doc),
                );
            });
        });
    });
});


// EOF
