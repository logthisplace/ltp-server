//


import * as Firestore from 'firebase/firestore';
import * as Sheet from './lib/sheet.js';
import * as Utils from './lib/utils.js';

import $ from 'jquery'; // eslint-disable-line id-length

function displaySheet(questionnaire, sheet) {
    const answers = sheet.answers;
    const questions = questionnaire.questions;
    for (const group of questions) {
        for (const question of group.questions) {
            const foo = Sheet.buildQa(question, answers, true);
            $('#sheet').append(foo);
        }
    }
}

async function getQuestionnaire(sheet) {
    const store = Utils.getFirestore();
    const coll = Firestore.collection(store, 'questionnaires');
    const questionnairesQuery = Firestore.query(
        coll,
        Firestore.where('type', '==', 'poi'),
        Firestore.orderBy('created_at', 'desc'),
        Firestore.limit(1),
    );
    const snap = await Firestore.getDocs(questionnairesQuery);
    snap.forEach((questionnaireDoc) => {
        const questionnaire = questionnaireDoc.data();
        displaySheet(questionnaire, sheet);
    });
}

async function getSheet(poiId, sheetId) {
    const store = Utils.getFirestore();
    const ref = Firestore.doc(store, 'pois', poiId, 'sheets', sheetId);
    const snap = await Firestore.getDoc(ref);
    if (snap.exists()) {
        const sheet = snap.data();
        getQuestionnaire(sheet);
    }
}

function setPoiLink(poiId) {
    $('#poi-link').attr('href', `/poi.html?id=${poiId}`);
}

function getParams() {
    const params = {};
    const url = new URL(window.location.href);
    params.poi = url.searchParams.get('poi');
    params.sheet = url.searchParams.get('sheet');
    return params;
}

async function init() {
    const params = getParams();
    setPoiLink(params.poi);
    await Utils.getFirebaseApp();
    getSheet(params.poi, params.sheet);
}

$(init);


// EOF
