//


import * as Firestore from 'firebase/firestore';
import * as Utils from './lib/utils.js';

import $ from 'jquery'; // eslint-disable-line id-length

// eslint-disable-next-line max-lines-per-function,max-statements
function addPoi(poiId, poi, questions) {
    const createdDate = poi.created_at.toDate();
    //
    const row = $('<tr>');
    const poiCell = $('<td>');
    const createdCell = $('<td>');
    const categoryMissingCell = $('<td>');
    const picturesMissingCell = $('<td>');
    const answersMissingCell = $('<td>');
    //
    const poiLink = $('<a>', {
        'href': `/poi.html?id=${poiId}`,
    });
    const createdTime = $('<time>', {
        'datetime': createdDate.toISOString(),
    });
    //
    createdTime.text(createdDate.toLocaleString());
    createdCell.append(createdTime);
    //
    poiLink.text(poiId);
    poiCell.append(poiLink);
    //
    // eslint-disable-next-line no-underscore-dangle
    if (poi._synth?.data?._report?._missing_category ?? false) {
        categoryMissingCell.text('MISS');
    }
    // eslint-disable-next-line no-underscore-dangle
    const missingPictures = poi._synth?.data?._report?._missing_pictures ?? [];
    if (missingPictures.length > 0) {
        const ul = $('<ul>');
        for (const uuid of missingPictures) {
            const li = $('<li>');
            li.text(questions[uuid]);
            ul.append(li);
        }
        picturesMissingCell.append(ul);
    }
    // eslint-disable-next-line no-underscore-dangle
    const missingAnswers = poi._synth?.data?._report?._missing_answers ?? [];
    if (missingAnswers.length > 0) {
        const ul = $('<ul>');
        for (const uuid of missingAnswers) {
            const li = $('<li>');
            li.text(questions[uuid]);
            ul.append(li);
        }
        answersMissingCell.append(ul);
    }
    //
    row.append(createdCell);
    row.append(poiCell);
    row.append(categoryMissingCell);
    row.append(picturesMissingCell);
    row.append(answersMissingCell);
    //
    $('#pois').append(row);
}

async function getPois(questions) {
    const store = Utils.getFirestore();
    const poisCollection = Firestore.collection(store, 'pois');
    const poisQuery = Firestore.query(
        poisCollection,
        Firestore.orderBy('created_at', 'desc'),
    );
    const snapshot = await Firestore.getDocs(poisQuery);
    let total = 0;
    snapshot.forEach((doc) => {
        const poi = doc.data();
        const poiId = doc.id;
        addPoi(poiId, poi, questions);
        ++total;
    });
    $('#total').text(total);
}

function getQuestions() {
    const store = Utils.getFirestore();
    const query = Firestore.query(
        Firestore.collection(store, 'questionnaires'),
        Firestore.where('type', '==', 'poi'),
        Firestore.orderBy('created_at', 'desc'),
        Firestore.limit(1),
    );
    return Firestore.getDocs(query).then((snap) => {
        const questions = {};
        snap.forEach((doc) => {
            const data = doc.data();
            for (const group of data.questions) {
                for (const question of group.questions) {
                    questions[question.uuid] = question.labels.en;
                }
            }
        });
        return questions;
    });
}

async function init() {
    await Utils.getFirebaseApp();
    const questions = await getQuestions();
    await getPois(questions);
}

$(init);


// EOF
