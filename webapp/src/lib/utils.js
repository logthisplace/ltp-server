//


/* global FIREBASE_EMULATORS_FIRESTORE_HOST */
/* global FIREBASE_EMULATORS_FIRESTORE_ON */
/* global FIREBASE_EMULATORS_FIRESTORE_PORT */
/* global FIREBASE_EMULATORS_STORAGE_HOST */
/* global FIREBASE_EMULATORS_STORAGE_ON */
/* global FIREBASE_EMULATORS_STORAGE_PORT */
/* global FIREBASE_EMULATORS_ON */
/* global GOOGLE_MAPS_API_KEY */

import * as FirebaseApp from 'firebase/app';
import * as FirebaseStorage from 'firebase/storage';
import * as Firestore from 'firebase/firestore';

let firebaseApp = null;
let firestore = null;
let gMapsApiKey = null;
let storage = null;

async function getFirebaseApp() {
    if (firebaseApp === null) {
        const response = await fetch('/__/firebase/init.json');
        const config = await response.json();
        const tmpApp = FirebaseApp.initializeApp(config);
        if (firebaseApp === null) {
            firebaseApp = tmpApp;
        }
    }
    return firebaseApp;
}

function getFirebaseStorage() {
    if (storage === null) {
        storage = FirebaseStorage.getStorage();
        if (FIREBASE_EMULATORS_ON && FIREBASE_EMULATORS_STORAGE_ON) {
            FirebaseStorage.connectStorageEmulator(
                storage,
                FIREBASE_EMULATORS_STORAGE_HOST,
                FIREBASE_EMULATORS_STORAGE_PORT,
            );
        }
    }
    return storage;
}

function getFirestore() {
    if (firestore === null) {
        firestore = Firestore.getFirestore();
        if (FIREBASE_EMULATORS_ON && FIREBASE_EMULATORS_FIRESTORE_ON) {
            Firestore.connectFirestoreEmulator(
                firestore,
                FIREBASE_EMULATORS_FIRESTORE_HOST,
                FIREBASE_EMULATORS_FIRESTORE_PORT,
            );
        }
    }
    return firestore;
}

function getMapsApiKey() {
    if (gMapsApiKey === null) {
        if (GOOGLE_MAPS_API_KEY) {
            gMapsApiKey = GOOGLE_MAPS_API_KEY;
        } else {
            gMapsApiKey = firebaseApp.options.apiKey;
        }
    }
    return gMapsApiKey;
}

export {
    getFirebaseApp,
    getFirebaseStorage,
    getFirestore,
    getMapsApiKey,
};


// EOF
