//


import * as FirebaseStorage from 'firebase/storage';
import * as Utils from './utils.js';

import $ from 'jquery'; // eslint-disable-line id-length

const TEXTS = {
    'no': 'No',
    'yes': 'Yes',
};

function getValue(answers, uuid) {
    let value = null;
    if (uuid in answers) {
        value = answers[uuid];
    }
    return value;
}

function getText(item) {
    let text = null;
    const labels = item.labels;
    if ('en' in labels) {
        text = labels.en;
    } else if ('sv' in labels) {
        text = labels.sv;
    }
    return text;
}

function buildBoolean( // eslint-disable-line max-params,max-statements
    question,
    answers,
    uuid,
    disabled,
) {
    const text = getText(question);
    const rawValue = answers[uuid];
    const par = $('<p>');
    const fieldset = $('<fieldset>');
    const yes = rawValue === true;
    fieldset.append($('<legend>').text(text));
    fieldset.append($('<input>', {
        'checked': !yes,
        'disabled': disabled,
        'id': `${uuid}-no`,
        'name': uuid,
        'type': 'radio',
        'value': 'no',
    }));
    fieldset.append($('<label>', {'for': `${uuid}-no`}).text(TEXTS.no));
    fieldset.append($('<br>'));
    fieldset.append($('<input>', {
        'checked': yes,
        'disabled': disabled,
        'id': `${uuid}-yes`,
        'name': uuid,
        'type': 'radio',
        'value': 'yes',
    }));
    fieldset.append($('<label>', {'for': `${uuid}-yes`}).text(TEXTS.yes));
    par.append(fieldset);
    return par;
}

function buildSelectMulti( // eslint-disable-line max-params,max-statements
    question,
    answers,
    uuid,
    disabled,
) {
    const text = getText(question);
    const rawValues = answers[uuid];
    const par = $('<p>');
    const fieldset = $('<fieldset>');
    fieldset.append($('<legend>').text(text));
    for (const option of question.values) {
        const optionUuid = option.uuid;
        const optionText = getText(option);
        const checked = rawValues && rawValues.includes(optionUuid);
        const input = $('<input>', {
            'checked': checked,
            'disabled': disabled,
            'id': optionUuid,
            'name': uuid,
            'type': 'checkbox',
            'value': optionUuid,
        });
        const label = $('<label>', {
            'for': optionUuid,
        });
        label.text(optionText);
        fieldset.append(input);
        fieldset.append(label);
        fieldset.append($('<br>'));
    }
    par.append(fieldset);
    return par;
}

function buildSelectSingle( // eslint-disable-line max-params,max-statements
    question,
    answers,
    uuid,
    disabled,
) {
    const text = getText(question);
    const rawValue = answers[uuid];
    const par = $('<p>');
    const fieldset = $('<fieldset>');
    fieldset.append($('<legend>').text(text));
    for (const option of question.values) {
        const optionUuid = option.uuid;
        const optionText = getText(option);
        const label = $('<label>', {
            'for': optionUuid,
        });
        const checked = rawValue === optionUuid;
        const input = $('<input>', {
            'checked': checked,
            'disabled': disabled,
            'id': optionUuid,
            'name': uuid,
            'type': 'radio',
            'value': optionUuid,
        });
        label.text(optionText);
        fieldset.append(input);
        fieldset.append(label);
        fieldset.append($('<br>'));
    }
    par.append(fieldset);
    return par;
}

async function setUrl(link, path) {
    const storage = Utils.getFirebaseStorage();
    const ref = FirebaseStorage.ref(storage, path);
    const url = await FirebaseStorage.getDownloadURL(ref);
    link.attr('href', url);
}

function buildPictures( // eslint-disable-line max-params,max-statements
    question,
    answers,
    uuid,
    disabled, // eslint-disable-line no-unused-vars
) {
    const rawValue = answers[uuid];
    const text = getText(question);
    //
    const par = $('<p>');
    const fieldset = $('<fieldset>');
    const legend = $('<legend>').text(text);
    //
    const paths = [];
    if (Array.isArray(rawValue)) {
        for (const item of rawValue) {
            if ($.type(item) === 'string') {
                paths.push(item);
            }
        }
    }
    //
    const ul = $('<ul>');
    for (const path of paths) {
        const li = $('<li>');
        const link = $('<a>').text(path);
        li.append(link);
        ul.append(li);
        setUrl(link, path);
    }
    //
    fieldset.append(ul);
    fieldset.append(legend);
    par.append(fieldset);
    return par;
}

function defaultBuilder( // eslint-disable-line max-params
    question,
    answers,
    uuid,
    disabled,
) {
    const text = getText(question);
    const value = getValue(answers, uuid);
    const content = `
        <p>
        <fieldset>
        <legend>${text}</legend>
        <input id="${uuid}" type="text" value="${value}" disabled="${disabled}">
        </fieldset>
        </p>`;
    const qa = $(content);
    return qa;
}

const builders = {
    'boolean': buildBoolean,
    'pictures': buildPictures,
    'select_multi': buildSelectMulti,
    'select_single': buildSelectSingle,
};

function buildQa( // eslint-disable-line max-statements
    question,
    answers,
    disabled,
) {
    let qa = null;
    const uuid = question.uuid;
    if ((answers && uuid in answers) || !disabled) {
        const type = question.type;
        let builder = null;
        if (type in builders) {
            builder = builders[type];
        } else {
            builder = defaultBuilder;
        }
        qa = builder(question, answers, uuid, disabled);
    } else {
        const text = getText(question);
        const content = `
            <p>
            <fieldset>
            <legend>${text}</legend>
            <span>-</span>
            </fieldset>
            </p>`;
        qa = $(content);
    }
    return qa;
}

export {
    buildQa,
};


// EOF
