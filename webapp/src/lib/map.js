//


/* global google */

const LOCATION = {
    'lat': 55.604981,
    'lng': 13.003822,
};

const BOUNDS = {
    'east': 13.15174583,
    'north': 55.67972462,
    'south': 55.48919432,
    'west': 12.71527775,
};

const COLORS = {
    'gray': '#949494',
    'white': '#ffffff',
};

const STYLES = [{
    'elementType': 'labels.icon',
    'featureType': 'poi',
    'stylers': [{'color': COLORS.gray}],
}, {
    'elementType': 'labels.text.fill',
    'featureType': 'poi',
    'stylers': [{'color': COLORS.gray}],
}, {
    'elementType': 'labels.text.stroke',
    'featureType': 'poi',
    'stylers': [{'color': COLORS.white}],
}, {
    'elementType': 'labels.icon',
    'featureType': 'transit.station',
    'stylers': [{'color': COLORS.gray}],
}, {
    'elementType': 'labels.text.fill',
    'featureType': 'transit.station',
    'stylers': [{'color': COLORS.gray}],
}, {
    'elementType': 'labels.text.stroke',
    'featureType': 'transit.station',
    'stylers': [{'color': COLORS.white}],
}];

const ZOOM = 12;

const CONFIG = {
    'center': LOCATION,
    'restriction': {
        'latLngBounds': BOUNDS,
        'strictBounds': false,
    },
    'scrollwheel': false,
    'styles': STYLES,
    'zoom': ZOOM,
};

function buildPoiMarker(map, poi) {
    const poiMeta = poi._meta; // eslint-disable-line no-underscore-dangle
    const poiLocation = poiMeta.data.geo.location;
    const marker = new google.maps.Marker({
        'map': map,
        'position': {
            'lat': poiLocation.latitude,
            'lng': poiLocation.longitude,
        },
        'title': poi.name,
    });
    return marker;
}

export {
    CONFIG,
    buildPoiMarker,
};


// EOF
