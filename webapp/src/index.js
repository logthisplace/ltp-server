//


/* global google */

import * as Firestore from 'firebase/firestore';
import * as Map from './lib/map.js';
import * as Utils from './lib/utils.js';

import $ from 'jquery'; // eslint-disable-line id-length

import {
    MarkerClusterer,
} from '@googlemaps/markerclusterer';

let infoWindow = null;
let map = null;

function showMarkerWindow(marker, poi, poiId) {
    const request = {
        'fields': [
            'name',
            'formatted_address',
        ],
        'placeId': poi.g_places_id,
    };
    const service = new google.maps.places.PlacesService(map);
    service.getDetails(request, (place, status_) => {
        if (status_ === google.maps.places.PlacesServiceStatus.OK) {
            const content = `<div><strong>${place.name}</strong></div>`
                + `<div>${place.formatted_address}</div>`
                + `<div><a href="/poi.html?id=${poiId}">View</a></div>`;
            infoWindow.setContent(content);
            infoWindow.open({
                'anchor': marker,
            });
        }
    });
}

async function showPois() {
    //
    const store = Utils.getFirestore();
    const poisRef = Firestore.collection(store, 'pois');
    const snapshot = await Firestore.getDocs(poisRef);
    //
    const clusterer = new MarkerClusterer({map});
    //
    snapshot.forEach((doc) => {
        const poi = doc.data();
        const marker = Map.buildPoiMarker(map, poi);
        marker.addListener('click', () => {
            infoWindow.close();
            showMarkerWindow(marker, poi, doc.id);
        });
        clusterer.addMarker(marker);
    });
}

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), Map.CONFIG);
    infoWindow = new google.maps.InfoWindow();
}

function preInitMap(apiKey) {
    const src = 'https://maps.googleapis.com/maps/api/js'
        + `?callback=initMap&key=${apiKey}&libraries=places`;
    const script = $('<script>', {
        src,
    });
    window.initMap = initMap;
    $('body').append(script);
}

async function init() {
    await Utils.getFirebaseApp();
    const apiKey = Utils.getMapsApiKey();
    if (apiKey) {
        preInitMap(apiKey);
        showPois();
    }
}

$(init);


// EOF
