//


import * as Firestore from 'firebase/firestore';
import * as Utils from './lib/utils.js';

import $ from 'jquery'; // eslint-disable-line id-length

function addSheet(poiId, sheetId, sheet) { // eslint-disable-line max-statements
    const updatedDate = sheet.updated_at.toDate();
    const row = $('<tr>');
    const sheetCell = $('<td>');
    const poiCell = $('<td>');
    const updatedCell = $('<td>');
    const sheetLink = $('<a>', {
        'href': `/sheet.html?poi=${poiId}&sheet=${sheetId}`,
    });
    const poiLink = $('<a>', {
        'href': `/poi.html?id=${poiId}`,
    });
    const updatedTime = $('<time>', {
        'datetime': updatedDate.toISOString(),
    });
    updatedTime.text(updatedDate.toLocaleString());
    sheetLink.text(sheetId);
    poiLink.text(poiId);
    sheetCell.append(sheetLink);
    poiCell.append(poiLink);
    updatedCell.append(updatedTime);
    row.append(updatedCell);
    row.append(sheetCell);
    row.append(poiCell);
    $('#sheets').append(row);
}

async function getSheets(filters) {
    const store = Utils.getFirestore();
    if ('poi' in filters) {
        const poiId = filters.poi;
        const poisQuery = Firestore.query(
            Firestore.collection(store, 'pois', poiId, 'sheets'),
            Firestore.orderBy('updated_at', 'desc'),
        );
        const snapshot = await Firestore.getDocs(poisQuery);
        snapshot.forEach((doc) => {
            addSheet(poiId, doc.id, doc.data());
        });
    } else {
        const sheetsQuery = Firestore.query(
            Firestore.collectionGroup(store, 'sheets'),
            Firestore.orderBy('updated_at', 'desc'),
        );
        const snapshot = await Firestore.getDocs(sheetsQuery);
        snapshot.forEach((doc) => {
            const sheet = doc.data();
            const sheetId = doc.id;
            const poiId = doc.ref.parent.parent.id;
            addSheet(poiId, sheetId, sheet);
        });
    }
}

function getFilters() {
    const filters = {};
    const known = [
        'poi',
    ];
    const url = new URL(window.location.href);
    for (const name of known) {
        if (url.searchParams.has(name)) {
            filters[name] = url.searchParams.get(name);
        }
    }
    return filters;
}

async function init() {
    const filters = getFilters();
    await Utils.getFirebaseApp();
    getSheets(filters);
}

$(init);


// EOF
