//


/* global google */

import * as Firestore from 'firebase/firestore';
import * as Map from './lib/map.js';
import * as Sheet from './lib/sheet.js';
import * as Utils from './lib/utils.js';

import $ from 'jquery'; // eslint-disable-line id-length

let map = null;

function setPoiInfo(poi) {
    const request = {
        'fields': [
            'name',
            'formatted_address',
        ],
        'placeId': poi.g_places_id,
    };
    const service = new google.maps.places.PlacesService(map);
    service.getDetails(request, (place, status_) => {
        if (status_ === google.maps.places.PlacesServiceStatus.OK) {
            $('#name').text(place.name);
            $('#address').text(place.formatted_address);
        }
    });
}

function setSheetsUrl(poiId) {
    const url = `/sheets.html?poi=${poiId}`;
    $('#sheets').attr('href', url);
}

function displaySheet(questionnaire, answers) {
    const questions = questionnaire.questions;
    for (const group of questions) {
        for (const question of group.questions) {
            const foo = Sheet.buildQa(question, answers, true);
            $('#sheet').append(foo);
        }
    }
}

async function getQuestionnaire(sheet) {
    const store = Utils.getFirestore();
    const coll = Firestore.collection(store, 'questionnaires');
    const questionnairesQuery = Firestore.query(
        coll,
        Firestore.where('type', '==', 'poi'),
        Firestore.orderBy('created_at', 'desc'),
        Firestore.limit(1),
    );
    const snap = await Firestore.getDocs(questionnairesQuery);
    snap.forEach((questionnaireDoc) => {
        const questionnaire = questionnaireDoc.data();
        displaySheet(questionnaire, sheet);
    });
}

async function getPoi(poiId) {
    const store = Utils.getFirestore();
    const poiRef = Firestore.doc(store, 'pois', poiId);
    const poiSnapshot = await Firestore.getDoc(poiRef);
    //
    if (poiSnapshot.exists()) {
        const poi = poiSnapshot.data();
        if (map) {
            Map.buildPoiMarker(map, poi);
            setPoiInfo(poi);
        }
        // eslint-disable-next-line no-underscore-dangle
        getQuestionnaire(poi._synth?.sheet);
    }
}

function getPoiId() {
    const url = new URL(window.location.href);
    const poiId = url.searchParams.get('id');
    return poiId;
}

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), Map.CONFIG);
}

function preInitMap(apiKey) {
    const src = 'https://maps.googleapis.com/maps/api/js'
        + `?callback=initMap&key=${apiKey}&libraries=places`;
    const script = $('<script>', {
        src,
    });
    window.initMap = initMap;
    $('body').append(script);
}

async function init() {
    const poiId = getPoiId();
    setSheetsUrl(poiId);
    await Utils.getFirebaseApp();
    const apiKey = Utils.getMapsApiKey();
    if (apiKey) {
        preInitMap(apiKey);
    }
    getPoi(poiId);
}

$(init);


// EOF
