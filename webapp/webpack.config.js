//


const CopyPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const Webpack = require('webpack');

require('dotenv').config();

const srcDir = path.resolve(__dirname, 'src');

const names = [
    'index',
    'poi',
    'pois',
    'sheet',
    'sheets',
];

function buildEntry() {
    const entry = {};
    for (const name of names) {
        entry[name] = {
            'dependOn': 'shared',
            'import': `./${name}.js`,
        };
    }
    entry.shared = [
        'firebase/app',
        'firebase/firestore',
        'firebase/storage',
        'jquery',
    ];
    return entry;
}

function getHtmls() {
    const htmls = [];
    for (const name of names) {
        htmls.push(new HtmlWebpackPlugin({
            'chunks': [
                name,
                'shared',
            ],
            'filename': `${name}.html`,
            'template': `${name}.hbs`,
        }));
    }
    return htmls;
}

function getDefinitions(env) {
    const definitions = new Webpack.DefinePlugin({
        'FIREBASE_EMULATORS_FIRESTORE_HOST': JSON.stringify('localhost'),
        'FIREBASE_EMULATORS_FIRESTORE_ON': true,
        'FIREBASE_EMULATORS_FIRESTORE_PORT': JSON.stringify(8080),
        'FIREBASE_EMULATORS_ON': env.emulators,
        'FIREBASE_EMULATORS_STORAGE_HOST': JSON.stringify('localhost'),
        'FIREBASE_EMULATORS_STORAGE_ON': true,
        'FIREBASE_EMULATORS_STORAGE_PORT': JSON.stringify(9199),
        'GOOGLE_MAPS_API_KEY': env.emulators
            ? JSON.stringify(process.env.GOOGLE_MAPS_API_KEY)
            : null,
    });
    return definitions;
}

module.exports = (env) => {
    const config = {
        'context': srcDir,
        'devtool': 'source-map',
        'entry': buildEntry(),
        'module': {
            'rules': [
                {
                    'loader': 'handlebars-loader',
                    'test': /\.hbs$/u,
                },
            ],
        },
        'plugins': [
            new CopyPlugin({
                'patterns': [
                    {
                        'context': srcDir,
                        'from': 'favicon.ico',
                    },
                ],
            }),
            getDefinitions(env),
            ...getHtmls(),
        ],
    };
    return config;
};


// EOF
