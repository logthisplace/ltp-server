module.exports = {
    'env': {
        'browser': true,
        'es2021': true,
    },
    'extends': 'eslint:all',
    'overrides': [{
        'env': {
            'node': true,
        },
        'files': [
            '.eslintrc.js',
            'webpack.config.js',
        ],
    }],
    'parserOptions': {
        'ecmaVersion': 12,
        'sourceType': 'module',
    },
    'reportUnusedDisableDirectives': true,
    'root': true,
    'rules': {
        'array-bracket-newline': ['error', 'consistent'],
        'array-element-newline': ['error', 'consistent'],
        'arrow-body-style': ['error', 'always'],
        'comma-dangle': ['error', 'always-multiline'],
        'func-style': ['error', 'declaration'],
        'function-call-argument-newline': ['error', 'consistent'],
        'newline-per-chained-call': ['off'],
        'no-extra-parens': ['error', 'all', {'nestedBinaryExpressions': false}],
        'no-magic-numbers': ['off'],
        'no-plusplus': ['off'],
        'no-ternary': ['off'],
        'object-shorthand': ['error', 'consistent-as-needed'],
        'one-var': ['error', 'never'],
        'operator-linebreak': ['error', 'before'],
        'padded-blocks': ['error', 'never'],
        'prefer-destructuring': ['off'],
        'quotes': ['error', 'single'],
        'space-before-function-paren': ['error', {
            'asyncArrow': 'always',
            'named': 'never',
        }],
    },
};
